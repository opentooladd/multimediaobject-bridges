/**
 * @jest-environment jsdom
 */

import { MOToDOM } from "./MOToDOM"

describe("MOToDOM bridge", () => {
  it("should append to document body if not a child", () => {
    const mo = {
      id: "test-mo",
      data: {},
      triggerHooks: jest.fn()
    }
    const driver = {
      appendDOMElement: jest.fn()
    }
    MOToDOM.appendToDocument.call(driver, mo)

    expect(driver.appendDOMElement).toHaveBeenCalledWith(undefined)
    expect(mo.triggerHooks).toHaveBeenCalledWith("domLoaded")
  })

  it("should append to parent if child", () => {
    const mo = {
      id: "test-mo",
      data: {
        MOParent: {
          data: {
            drivers: {
              DOM: {
                element: {
                  id: "element"
                }
              }
            }
          }
        }
      },
      triggerHooks: jest.fn()
    }
    const driver = {
      appendDOMElement: jest.fn()
    }
    MOToDOM.appendToDocument.call(driver, mo)

    expect(driver.appendDOMElement).toHaveBeenCalledWith(mo.data.MOParent.data.drivers.DOM.element)
    expect(mo.triggerHooks).toHaveBeenCalledWith("domLoaded")
  })

  it("should throw because data is not there", () => {
    const mo = {
      id: "test-mo",
      triggerHooks: jest.fn()
    }
    expect(() => MOToDOM.appendToDocument(mo)).toThrowError(
      new TypeError("Cannot read properties of undefined (reading 'MOParent')")
    )
  })
})
