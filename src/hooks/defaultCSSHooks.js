const defaultCSSHooks = (CSSToDOM) => ([
  {
    type: "init",
    fn: CSSToDOM.styleToDOM,
  },
  {
    type: "update",
    fn: CSSToDOM.styleToDOM,
  }
])

export { defaultCSSHooks }
