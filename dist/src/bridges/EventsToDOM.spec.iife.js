(function () {
  'use strict';

  const EventsToDOM = {
    bindMOEventsToDOMEvents: function(mo) {
      const publishList = {};
      Object.keys(mo.events).forEach((eventKey) => {
        const topic = `${mo.name}/${eventKey}`;
        this.subscribe(topic, mo.events[eventKey], {
          context: mo
        });
        publishList[eventKey] = (evt) => this.publish(topic, evt);
      });
      mo.updateDriver("DOM", "bindDOMEvents", publishList);
    },
    getWindowConfiguration: function(mo) {
      const CSSDriver = mo.getDriver("CSS");
      CSSDriver.namespace._style = {
        css: Object.assign({}, CSSDriver.namespace.style),
        applied: true,
      };
      return Object.entries(CSSDriver.namespace.window).map((entry) => {
        return {
          test: (width, height) => eval(entry[0]),
          applied: false,
          css: entry[1],
        }
      })
    },
    bindToWindowResize: function(mo) {
      const CSSDriver = mo.getDriver("CSS");
      if (!this.namespace.MOParent) {
        window.addEventListener(
          "resize",
          () => this.publish("window/resize", { width: window.innerWidth, height: window.innerHeight })
        );
      }
      if (CSSDriver && CSSDriver.namespace.window) {
        CSSDriver.namespace._window = EventsToDOM.getWindowConfiguration(mo);
        this.subscribe("window/resize", EventsToDOM.onWindowResize, { context: mo });
      }
      this.publish("window/resize", { width: window.innerWidth, height: window.innerHeight });
    },
    onWindowResize: function(screen) {
      const CSSDriver = this.getDriver("CSS");
      CSSDriver.namespace._window.forEach((configuration) => {
        if (configuration.test.apply(this, [screen.width, screen.height])) {
          if (!configuration.applied) {
            this.updateDriver("CSS", "applyStyle", configuration.css);
            configuration.applied = true;
            CSSDriver.namespace._style.applied = false;
          }
        } else if (
          (!configuration.test.apply(this, [screen.width, screen.height])) &&
          configuration.applied
        ) {
          configuration.applied = false;
          if (!CSSDriver.namespace._style.applied) {
            this.updateDriver("CSS", "applyStyle", CSSDriver.namespace._style.css);
            CSSDriver.namespace._style.applied = true;
          }
        } 
      });
    }
  };

  /**
   * @jest-environment jsdom
   */

  describe("EventsToDOM", () => {
    it("should bind a DOM event to a DOMElement", () => {
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn(),
        namespace: {}
      };
      const mo = {
        name: "test-name",
        events: {
          click: jest.fn(),
          mouseover: jest.fn(),
        },
        updateDriver: jest.fn().mockImplementation((_d, _fn, publishList) => {
          driver.namespace.publishList = publishList;
        })
      };

      EventsToDOM.bindMOEventsToDOMEvents.call(driver, mo);

      expect(driver.subscribe).toHaveBeenCalledWith("test-name/click", mo.events.click, { context: mo });
      expect(driver.subscribe).toHaveBeenCalledWith("test-name/mouseover", mo.events.mouseover, { context: mo });
      expect(driver.subscribe.mock.contexts[0]).toEqual(driver);
      expect(mo.updateDriver).toHaveBeenCalledWith("DOM", "bindDOMEvents", {
        click: expect.any(Function),
        mouseover: expect.any(Function)
      });
      driver.namespace.publishList.click({ target: "test" });
      expect(driver.publish).toHaveBeenCalledWith("test-name/click", { target: "test" });
    });

    it("should get window configuration", () => {
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn()
      };
      const windowCSS = {
        "color": "red",
        "font-size": "14px"
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
          window: {
            "width <= 10 && height > 5": {
              ...windowCSS
            }
          }
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver)
      };

      const result = EventsToDOM.getWindowConfiguration.call(driver, mo);

      expect(result).toEqual([
        {
          test: expect.any(Function),
          applied: false,
          css: {
            ...windowCSS
          }
        },
      ]);
      expect(CSSDriver.namespace._style).toEqual({
        css: CSSDriver.namespace.style,
        applied: true
      });
      expect(result[0].test(9, 6)).toBeTruthy();
      expect(result[0].test(11, 6)).toBeFalsy();
      expect(result[0].test()).toBeFalsy();
    });


    it("should execute malicious code... eval you know, but it is more interesting like that", () => {
      console.log = jest.fn();
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn()
      };
      const windowCSS = {
        "color": "red",
        "font-size": "14px"
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
          window: {
            "console.log('test')": {
              ...windowCSS
            }
          }
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver)
      };

      const result = EventsToDOM.getWindowConfiguration.call(driver, mo);
      result[0].test();
      expect(console.log).toHaveBeenCalledWith("test");
      console.log.mockRestore();
    });


    it("should throw", () => {
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn()
      };
      const windowCSS = {
        "color": "red",
        "font-size": "14px"
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
          window: {
            "throw new Error('test')": {
              ...windowCSS
            }
          }
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver)
      };

      const result = EventsToDOM.getWindowConfiguration.call(driver, mo);
      expect(() => result[0].test()).toThrowError(new Error("test"));
    });


    it("should get window configuration", () => {
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn(),
        namespace: {}
      };
      const windowCSS = {
        "color": "red",
        "font-size": "14px"
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
          window: {
            "width <= 10 && height > 5": {
              ...windowCSS
            }
          }
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver)
      };

      Object.defineProperty(window, "innerHeight", {
        writable: true,
        configurable: true,
        value: 100,
      });
      Object.defineProperty(window, "innerWidth", {
        writable: true,
        configurable: true,
        value: 100,
      });
      window.dispatchEvent(new Event("resize"));

      const windowConf = EventsToDOM.getWindowConfiguration.call(driver, mo);
      EventsToDOM.bindToWindowResize.call(driver, mo);

      expect(JSON.stringify(CSSDriver.namespace._window)).toEqual(JSON.stringify(windowConf));
      expect(driver.subscribe).toHaveBeenCalledWith("window/resize", EventsToDOM.onWindowResize, { context: mo });
      expect(driver.publish).toHaveBeenCalledWith("window/resize", { width: 100, height: 100 });

      window.dispatchEvent(new Event("resize"));
      expect(driver.publish).toHaveBeenCalledWith("window/resize", { width: 100, height: 100 });
    });

    it("should not apply configuration if no dependencies", () => {
      jest.fn();
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn(),
        namespace: {
          MOParent: {}
        }
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver)
      };

      EventsToDOM.bindToWindowResize.call(driver, mo);
      
      expect(CSSDriver.namespace._style).toBeUndefined();
    });

    it("should apply style if condition are met", () => {
      const driver = {
        publish: jest.fn(),
        subscribe: jest.fn(),
        namespace: {}
      };
      const windowCSS = {
        "color": "red",
        "font-size": "14px"
      };
      const CSSDriver = {
        namespace: {
          style: {
            "border": "1px solid black",
          },
          window: {
            "width <= 100 && height > 50": {
              ...windowCSS
            }
          }
        }
      };
      const mo = {
        name: "test-name",
        getDriver: jest.fn().mockReturnValue(CSSDriver),
        updateDriver: jest.fn()
      };

      const windowConf = EventsToDOM.getWindowConfiguration.call(driver, mo);
      EventsToDOM.bindToWindowResize.call(driver, mo);
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 100 });

      expect(mo.updateDriver).toHaveBeenCalledWith("CSS", "applyStyle", windowConf[0].css);
      expect(mo.updateDriver).toHaveBeenCalledTimes(1);
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 100 });
      expect(mo.updateDriver).toHaveBeenCalledTimes(1);
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 48 });
      expect(mo.updateDriver).toHaveBeenCalledWith("CSS", "applyStyle", CSSDriver.namespace.style);
      expect(mo.updateDriver).toHaveBeenCalledTimes(2);
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 48 });
      expect(mo.updateDriver).toHaveBeenCalledTimes(2);
      expect(CSSDriver.namespace._style.applied).toBeTruthy();
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 100 });
      expect(mo.updateDriver).toHaveBeenCalledTimes(3);

      // if applied is still true
      // it won't apply
      // should not happen
      CSSDriver.namespace._style.applied = true;
      EventsToDOM.onWindowResize.call(mo, { width: 100, height: 48 });
      expect(mo.updateDriver).toHaveBeenCalledTimes(3);
    });
  });

})();
