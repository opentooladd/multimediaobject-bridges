const CSSToDOM = {
  styleToDOM: function(mo) {
    if (mo) {
      this.toStyleString();
      mo.updateDriver("DOM", "setAttributes", {
        style: this.namespace.styleString,
        id: mo.name
      });
    }
  }
};

/**
 * @jest-environment jsdom
 */

describe("CSSToDOM", () => {
  it("should call updateDriver with styleString", () => {
    const mo = {
      name: "test-name",
      updateDriver: jest.fn()
    };
    const driver = {
      namespace: {},
      toStyleString: jest.fn().mockImplementation(() => {
        driver.namespace.styleString = "test";
      })
    };

    CSSToDOM.styleToDOM.call(driver, mo);

    expect(driver.toStyleString).toHaveBeenCalled();
    expect(mo.updateDriver).toHaveBeenCalledWith("DOM", "setAttributes", {
      style: driver.namespace.styleString,
      id: mo.name
    });
  });
  it("should call nothin if no MO", () => {
    const driver = {
      namespace: {},
      toStyleString: jest.fn().mockImplementation(() => {
        driver.namespace.styleString = "test";
      })
    };

    CSSToDOM.styleToDOM.call(driver, undefined);

    expect(driver.toStyleString).not.toHaveBeenCalled();
  });
  it("should throw if no namespace", () => {
    const mo = {};
    const driver = {
      toStyleString: jest.fn().mockImplementation(() => {
        driver.namespace.styleString = "test";
      })
    };

    expect(() => CSSToDOM.styleToDOM.call(driver, mo)).toThrow();
  });
});
