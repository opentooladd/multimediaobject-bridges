const MOToDOM = {
  appendToDocument: function(mo) {
    const parent = mo.data.MOParent ? mo.data.MOParent.data.drivers.DOM.element : mo.data.drivers.DOM.DOMParent;
    this.appendDOMElement(parent);
    mo.triggerHooks("domLoaded");
  }
};

export { MOToDOM };
