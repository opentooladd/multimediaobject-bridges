const EventsToDOM = {
  bindMOEventsToDOMEvents: function(mo) {
    const publishList = {};
    Object.keys(mo.events).forEach((eventKey) => {
      const topic = `${mo.name}/${eventKey}`;
      this.subscribe(topic, mo.events[eventKey], {
        context: mo
      });
      publishList[eventKey] = (evt) => this.publish(topic, evt);
    });
    mo.updateDriver("DOM", "bindDOMEvents", publishList);
  },
  getWindowConfiguration: function(mo) {
    const CSSDriver = mo.getDriver("CSS");
    CSSDriver.namespace._style = {
      css: Object.assign({}, CSSDriver.namespace.style),
      applied: true,
    };
    return Object.entries(CSSDriver.namespace.window).map((entry) => {
      return {
        test: (width, height) => eval(entry[0]),
        applied: false,
        css: entry[1],
      }
    })
  },
  bindToWindowResize: function(mo) {
    const CSSDriver = mo.getDriver("CSS");
    if (!this.namespace.MOParent) {
      window.addEventListener(
        "resize",
        () => this.publish("window/resize", { width: window.innerWidth, height: window.innerHeight })
      );
    }
    if (CSSDriver && CSSDriver.namespace.window) {
      CSSDriver.namespace._window = EventsToDOM.getWindowConfiguration(mo);
      this.subscribe("window/resize", EventsToDOM.onWindowResize, { context: mo });
    }
    this.publish("window/resize", { width: window.innerWidth, height: window.innerHeight });
  },
  onWindowResize: function(screen) {
    const CSSDriver = this.getDriver("CSS");
    CSSDriver.namespace._window.forEach((configuration) => {
      if (configuration.test.apply(this, [screen.width, screen.height])) {
        if (!configuration.applied) {
          this.updateDriver("CSS", "applyStyle", configuration.css);
          configuration.applied = true;
          CSSDriver.namespace._style.applied = false;
        }
      } else if (
        (!configuration.test.apply(this, [screen.width, screen.height])) &&
        configuration.applied
      ) {
        configuration.applied = false;
        if (!CSSDriver.namespace._style.applied) {
          this.updateDriver("CSS", "applyStyle", CSSDriver.namespace._style.css);
          CSSDriver.namespace._style.applied = true;
        }
      } 
    });
  }
};

export { EventsToDOM };
