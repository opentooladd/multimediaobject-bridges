(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.MOToDOM = {}));
})(this, (function (exports) { 'use strict';

  const MOToDOM = {
    appendToDocument: function(mo) {
      const parent = mo.data.MOParent ? mo.data.MOParent.data.drivers.DOM.element : mo.data.drivers.DOM.DOMParent;
      this.appendDOMElement(parent);
      mo.triggerHooks("domLoaded");
    }
  };

  exports.MOToDOM = MOToDOM;

}));
