'use strict';

const CSSToDOM = {
  styleToDOM: function(mo) {
    if (mo) {
      this.toStyleString();
      mo.updateDriver("DOM", "setAttributes", {
        style: this.namespace.styleString,
        id: mo.name
      });
    }
  }
};

exports.CSSToDOM = CSSToDOM;
