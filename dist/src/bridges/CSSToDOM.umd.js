(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.CSSToDOM = {}));
})(this, (function (exports) { 'use strict';

  const CSSToDOM = {
    styleToDOM: function(mo) {
      if (mo) {
        this.toStyleString();
        mo.updateDriver("DOM", "setAttributes", {
          style: this.namespace.styleString,
          id: mo.name
        });
      }
    }
  };

  exports.CSSToDOM = CSSToDOM;

}));
