(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
})((function () { 'use strict';

  const MOToDOM = {
    appendToDocument: function(mo) {
      const parent = mo.data.MOParent ? mo.data.MOParent.data.drivers.DOM.element : mo.data.drivers.DOM.DOMParent;
      this.appendDOMElement(parent);
      mo.triggerHooks("domLoaded");
    }
  };

  /**
   * @jest-environment jsdom
   */

  describe("MOToDOM bridge", () => {
    it("should append to document body if not a child", () => {
      const mo = {
        id: "test-mo",
        data: {},
        triggerHooks: jest.fn()
      };
      const driver = {
        appendDOMElement: jest.fn()
      };
      MOToDOM.appendToDocument.call(driver, mo);

      expect(driver.appendDOMElement).toHaveBeenCalledWith(undefined);
      expect(mo.triggerHooks).toHaveBeenCalledWith("domLoaded");
    });

    it("should append to parent if child", () => {
      const mo = {
        id: "test-mo",
        data: {
          MOParent: {
            data: {
              drivers: {
                DOM: {
                  element: {
                    id: "element"
                  }
                }
              }
            }
          }
        },
        triggerHooks: jest.fn()
      };
      const driver = {
        appendDOMElement: jest.fn()
      };
      MOToDOM.appendToDocument.call(driver, mo);

      expect(driver.appendDOMElement).toHaveBeenCalledWith(mo.data.MOParent.data.drivers.DOM.element);
      expect(mo.triggerHooks).toHaveBeenCalledWith("domLoaded");
    });

    it("should throw because data is not there", () => {
      const mo = {
        id: "test-mo",
        triggerHooks: jest.fn()
      };
      expect(() => MOToDOM.appendToDocument(mo)).toThrowError(
        new TypeError("Cannot read properties of undefined (reading 'MOParent')")
      );
    });
  });

}));
