'use strict';

const defaultAnimationHooks = () => [
  {
    type: "init",
    fn: function(mo) {
      this.namespace.mo = mo;
      const addChilds = (rootMo, rootNamespace) => {
        rootMo.childs.filter((c) => c.data.drivers.Animation).forEach((c) => {
          c.data.drivers.Animation.targets.push(c.data.drivers.DOM.element);
          rootNamespace.childs.push(c.data.drivers.Animation);
          if (rootMo.childs) addChilds(c, rootNamespace);
        });
      };
      if (!this.namespace.MOParent) {
        this.namespace.targets.push(mo.data.drivers.DOM.element);
        addChilds(mo, this.namespace);
      }
      this.updateInstance();

      if (mo.sendAnimationData) mo.sendAnimationData();
    }
  }
];

exports.defaultAnimationHooks = defaultAnimationHooks;
