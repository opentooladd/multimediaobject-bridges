(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.defaultDOMHooks = {}));
})(this, (function (exports) { 'use strict';

  const defaultDOMHooks = (MOToDOM) => ([
    {
      type: "init",
      fn: MOToDOM.appendToDocument,
    }
  ]);

  exports.defaultDOMHooks = defaultDOMHooks;

}));
