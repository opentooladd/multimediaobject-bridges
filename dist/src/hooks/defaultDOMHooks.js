const defaultDOMHooks = (MOToDOM) => ([
  {
    type: "init",
    fn: MOToDOM.appendToDocument,
  }
]);

export { defaultDOMHooks };
