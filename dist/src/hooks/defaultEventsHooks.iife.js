var defaultEventsHooks = (function (exports) {
  'use strict';

  const defaultEventsHooks = (EventsToDOM) => [
    {
      type: "init",
      fn: EventsToDOM.bindMOEventsToDOMEvents
    },
    {
      type: "init",
      fn: EventsToDOM.bindToWindowResize
    }
  ];

  exports.defaultEventsHooks = defaultEventsHooks;

  return exports;

})({});
