const defaultEventsHooks = (EventsToDOM) => [
  {
    type: "init",
    fn: EventsToDOM.bindMOEventsToDOMEvents
  },
  {
    type: "init",
    fn: EventsToDOM.bindToWindowResize
  }
];

export { defaultEventsHooks };
