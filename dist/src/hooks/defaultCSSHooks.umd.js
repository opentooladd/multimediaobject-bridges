(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
  typeof define === 'function' && define.amd ? define(['exports'], factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.defaultCSSHooks = {}));
})(this, (function (exports) { 'use strict';

  const defaultCSSHooks = (CSSToDOM) => ([
    {
      type: "init",
      fn: CSSToDOM.styleToDOM,
    },
    {
      type: "update",
      fn: CSSToDOM.styleToDOM,
    }
  ]);

  exports.defaultCSSHooks = defaultCSSHooks;

}));
