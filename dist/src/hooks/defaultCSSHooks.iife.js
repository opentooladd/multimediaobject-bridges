var defaultCSSHooks = (function (exports) {
  'use strict';

  const defaultCSSHooks = (CSSToDOM) => ([
    {
      type: "init",
      fn: CSSToDOM.styleToDOM,
    },
    {
      type: "update",
      fn: CSSToDOM.styleToDOM,
    }
  ]);

  exports.defaultCSSHooks = defaultCSSHooks;

  return exports;

})({});
