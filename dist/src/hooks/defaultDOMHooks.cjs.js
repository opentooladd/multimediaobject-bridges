'use strict';

const defaultDOMHooks = (MOToDOM) => ([
  {
    type: "init",
    fn: MOToDOM.appendToDocument,
  }
]);

exports.defaultDOMHooks = defaultDOMHooks;
